<?php

namespace App\Http\Controllers;

class UserController extends Controller
{
    use \App\Traits\ApiControllerTrait;

    protected $model;
    protected $relationships = [''];

    public function __construct(\App\Entities\User $model)
    {
        $this->model = $model;
    }
}
