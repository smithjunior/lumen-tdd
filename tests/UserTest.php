<?php


use Laravel\Lumen\Testing\DatabaseMigrations;

class UserTest extends \TestCase
{
    use DatabaseMigrations;

    /**
     * A basic test to validate the create use on databse!.
     *
     * @test
     */
    public function testCreateUser()
    {
        $user = factory(App\Entities\User::class)->make();

        $this->call('post', '/api/user', $user->toArray());

        $response = (array) json_decode($this->response->content());

        $this->assertResponseOk();
        $this->assertArrayHasKey('data', $response);
    }

    /**
     * A basic test for the make the assert to a user has been created on database!
     *
     * @test
     */
    public function testCreateEmailDuplicateFail()
    {
        $data = [
            'name' => 'Name 01',
            'email' => 'email@example.com',
            'password' => '123456',
        ];

        $this->call('post', '/api/user', $data);
        $this->call('post', '/api/user', $data);

        $response = (array) json_decode($this->response->content(), true);

        $this->assertResponseStatus(422);

        $this->assertArrayHasKey('modelError', $response);
        $this->assertArrayHasKey('email', $response['modelError']);
        $this->assertEquals('The email has already been taken.', $response['modelError']['email'][0]);
    }

    /**
     * A basic test for the make the assert to a user is valid or not!
     *
     * @test
     */
    public function testCreatePasswordIsLittleFail()
    {
        $data = [
            'name' => 'Name 01',
            'email' => 'email@example.com',
            'password' => '123',
        ];

        $this->call('post', '/api/user', $data);

        $response = (array) json_decode($this->response->content(), true);

        $this->assertResponseStatus(422);
        $this->assertArrayHasKey('modelError', $response);
        $this->assertArrayHasKey('password', $response['modelError']);
        $this->assertEquals('The password must be between 6 and 20 characters.', $response['modelError']['password'][0]);
    }
}
